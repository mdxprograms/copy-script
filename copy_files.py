import glob
import shutil
import os
import fnmatch
import time # will need this to compare modified date


def compare_dir(src_dir, dst_dir, ftype=''):
    dst_files = []
    src_files = []

    # check destination directory and add a list of existing files
    if os.listdir(dst_dir):
        for root, dirnames, filenames in os.walk(dst_dir):
            for filename in fnmatch.filter(filenames, '*' + ftype):
                dst_files.append(filename)

    # check source directory and add a list of existing files
    if os.listdir(src_dir):
        for root, dirnames, filenames in os.walk(src_dir):
            for filename in fnmatch.filter(filenames, '*' + ftype):
                src_files.append(filename)

    # compare directories existing files
    if set(src_files) == set(dst_files):
        return True
    else:
        return False


def copy_files(src_dir, dst_dir, ftype='', recurs=False):

    # if the function returns unequal directories
    if not compare_dir(src_dir, dst_dir, ftype):

        # * is used to sub directory search
        if recurs:
            for src_file in glob.iglob(os.path.join(src_dir, '*', '*' + ftype)): # all files matching ftype, recursive
                shutil.copy(src_file, dst_dir)
        else:
            for src_file in glob.iglob(os.path.join(src_dir, '*' + ftype)): # all files matching ftype, non-recursive
                shutil.copy(src_file, dst_dir)
    else:
        print
        print "Directories match"


def main():
    src_dir = raw_input("Source Directory: ")
    dst_dir = raw_input("Destination Directory: ")
    ftype   = raw_input("File Extension to match(.txt, .pdf, .xml): ")
    recurs  = raw_input("Does the source directory contain sub directories? Y/N: ")

    if str.lower(recurs) == 'y' or 'yes':
        recurs = True
    else:
        recurs = False

    if src_dir and dst_dir and ftype and recurs:
        copy_files(src_dir, dst_dir, ftype, recurs)
    else:
        print
        print "Please enter all values: Source Directory, Destination Directory, File Type, " \
              "and Y/N If source directory contains sub directories"

if __name__ == "__main__":
    main()